package de.memoria.app.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import de.memoria.app.R;
import de.memoria.app.database.DatabaseHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by maikel on 30.10.14.
 */
public class ReminderAdapter extends CursorAdapter {

    public ReminderAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.reminder_item, parent, false);

        return retView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewTitle = (TextView) view.findViewById(R.id.reminderItemTitle);
        textViewTitle.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.REMINDER_TABLE_COLUMN_TITLE)));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(cursor.getString(cursor.getColumnIndex(DatabaseHelper.REMINDER_TABLE_COLUMN_DATE)));

            TextView textViewDay = (TextView) view.findViewById(R.id.reminderItemDay);
            textViewDay.setText(new SimpleDateFormat("dd").format(date));

            TextView textViewMonth = (TextView) view.findViewById(R.id.reminderItemMonth);
            textViewMonth.setText(new SimpleDateFormat("MMM").format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
