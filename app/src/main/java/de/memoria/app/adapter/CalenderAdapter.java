package de.memoria.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.CalendarContract.Events;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageButton;
import android.widget.TextView;
import de.memoria.app.R;
import de.memoria.app.model.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by maikel on 30.10.14.
 */
public class CalenderAdapter extends CursorAdapter {

    private Context mContext;
    private String mContactName;

    public CalenderAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
        mContext = context;
    }

    public void setContactName(String contactName) {
        mContactName = contactName;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.detail_calendar_item, parent, false);

        return retView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        TextView textViewTitle = (TextView) view.findViewById(R.id.detailCalendarItemTitle);
        textViewTitle.setText(cursor.getString(EventQuery.TITLE));

        ImageButton share = (ImageButton) view.findViewById(R.id.detailCalendarItemShare);

        try {
            final Calendar date = Calendar.getInstance();
            date.setTimeInMillis(cursor.getLong(EventQuery.DTSTART));

            TextView textViewDay = (TextView) view.findViewById(R.id.detailCalendarItemDay);
            textViewDay.setText(new SimpleDateFormat("dd").format(date.getTime()));

            TextView textViewMonth = (TextView) view.findViewById(R.id.detailCalendarItemMonth);
            textViewMonth.setText(new SimpleDateFormat("MMM yyyy").format(date.getTime()));

            TextView textViewTime = (TextView) view.findViewById(R.id.detailCalendarItemTime);
            textViewTime.setText(new SimpleDateFormat("HH:mm").format(date.getTime()) + " Uhr");

            final SimpleDateFormat df1 = new SimpleDateFormat("EEEE");
            final SimpleDateFormat df2 = new SimpleDateFormat("dd.MM.yyy");
            final SimpleDateFormat df3 = new SimpleDateFormat("HH:mm");

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sendIntent = new Intent(Intent.ACTION_SEND);
                    sendIntent.putExtra(
                            Intent.EXTRA_TEXT,
                            String.format(
                                    context.getString(R.string.share_text),
                                    mContactName,
                                    df1.format(date.getTime()),
                                    df2.format(date.getTime()),
                                    df3.format(date.getTime()),
                                    User.getInstance().getName()
                            )
                    );
                    sendIntent.setType("text/plain");
                    sendIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(sendIntent);
                }
            });
        } catch (Exception e) {
            view.setVisibility(View.GONE);
            e.printStackTrace();
        }
    }

    public interface EventQuery {
        String[] PROJECTION = new String[]{
            Events._ID,
            Events.TITLE,
            Events.DTSTART
        };

        String SORT =
                Events.DTSTART +","+
                Events.TITLE;

        String SELECTION =
                Events._ID + " IN ( ? )";

        int ID = 0;
        int TITLE = 1;
        int DTSTART = 2;
    }
}
