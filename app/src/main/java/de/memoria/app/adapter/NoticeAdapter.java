package de.memoria.app.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import de.memoria.app.R;
import de.memoria.app.database.DatabaseHelper;

/**
 * Created by maikel on 30.10.14.
 */
public class NoticeAdapter extends CursorAdapter {

    public NoticeAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View retView = inflater.inflate(R.layout.detail_notice_item, parent, false);

        return retView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView textViewNotice = (TextView) view.findViewById(R.id.detailNoticeText);
        textViewNotice.setText(cursor.getString(cursor.getColumnIndex(DatabaseHelper.NOTICE_TABLE_COLUMN_TEXT)));
    }
}
