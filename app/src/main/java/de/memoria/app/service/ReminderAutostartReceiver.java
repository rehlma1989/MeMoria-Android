package de.memoria.app.service;

/**
 * Created by maikel on 02.11.14.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Autostart the service
 */
public class ReminderAutostartReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent)
    {
        Intent pushIntent = new Intent(context, ReminderService.class);
        context.startService(pushIntent);
    }
}