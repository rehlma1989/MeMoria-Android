package de.memoria.app.util;

import android.app.ActivityManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CalendarContract;
import android.provider.CalendarContract.Calendars;
import android.util.Log;
import de.memoria.app.R;
import de.memoria.app.adapter.CalenderAdapter;

import java.util.HashMap;

public class Utils {

    public static boolean isServiceRunning(Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if ("de.memoria.app.service".equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static class Calendar {

        public static int getIdByName(Context context, String calendarName) {
            Cursor cursor = context.getContentResolver().query(
                    CalendarContract.Calendars.CONTENT_URI,
                    CalenderQuery.PROJECTION,
                    CalenderQuery.SELECTION,
                    new String[]{calendarName},
                    null
            );

            while (cursor.moveToNext()) {
                Log.d("Calendar: getIdByName", cursor.getInt(CalenderQuery.ID) + "");
                if (cursor.getString(CalenderQuery.NAME).equals(calendarName)) return cursor.getInt(CalenderQuery.ID);
            }
            return -1;
        }

        public static HashMap<Integer, String> isCalendarAvailable(Context context) {
            HashMap<Integer, String> calenders = new HashMap<Integer, String>();
            Cursor cursor = context.getContentResolver().query(
                    CalendarContract.Calendars.CONTENT_URI,
                    CalenderQuery.PROJECTION,
                    CalenderQuery.SELECTION,
                    null,
                    Calendars._ID
            );

            while (cursor.moveToNext()) {
                calenders.put(cursor.getInt(CalenderQuery.ID), cursor.getString(CalenderQuery.NAME));
            }
            cursor.close();
            return calenders;
        }

        public static void addCalendar(Context context, String calendarName) {
            ContentValues cv = new ContentValues();

            cv.put(Calendars.ACCOUNT_NAME, context.getPackageName());
            cv.put(Calendars.ACCOUNT_TYPE, CalendarContract.ACCOUNT_TYPE_LOCAL);
            cv.put(Calendars.NAME, calendarName);
            cv.put(Calendars.CALENDAR_DISPLAY_NAME, calendarName);
            cv.put(Calendars.CALENDAR_COLOR, context.getResources().getColor(R.color.memoria_green));

            //user can only read the calendar
            cv.put(Calendars.CALENDAR_ACCESS_LEVEL, Calendars.CAL_ACCESS_OWNER);
            cv.put(Calendars.OWNER_ACCOUNT, context.getPackageName());
            cv.put(Calendars.VISIBLE, 0);
            cv.put(Calendars.SYNC_EVENTS, 1);

            Uri CAL_URI = CalendarContract.Calendars.CONTENT_URI
                    .buildUpon()
                    .appendQueryParameter(CalendarContract.CALLER_IS_SYNCADAPTER, "true")
                    .appendQueryParameter(Calendars.ACCOUNT_NAME, context.getPackageName())
                    .appendQueryParameter(Calendars.ACCOUNT_TYPE,
                            CalendarContract.ACCOUNT_TYPE_LOCAL)
                    .build();

            context.getContentResolver().insert(CAL_URI, cv);
        }

        private interface CalenderQuery {
            String[] PROJECTION = new String[]{
                    CalendarContract.Calendars._ID,
                    CalendarContract.Calendars.NAME
            };

            String SELECTION =
                    Calendars.SYNC_EVENTS+"=1";

            int ID = 0;
            int NAME = 1;
        }

        public static Cursor getEventByEventId(Context context, long id) {
            Cursor eventCursor = context.getContentResolver().query(
                    CalendarContract.Events.CONTENT_URI,
                    CalenderAdapter.EventQuery.PROJECTION,
                    CalendarContract.Events._ID + " = ?",
                    new String[]{String.valueOf(id)},
                    null);
            return eventCursor;
        }
    }
}
