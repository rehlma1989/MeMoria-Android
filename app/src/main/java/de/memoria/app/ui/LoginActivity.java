package de.memoria.app.ui;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import de.memoria.app.R;
import de.memoria.app.model.User;
import de.memoria.app.model.Wrapper;
import de.memoria.app.service.ReminderService;
import de.memoria.app.util.Utils;
import de.memoria.app.util.WsseToken;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;


public class LoginActivity extends Activity implements View.OnClickListener {

    private static final String loginUrl = "https://www.memoria-app.de/api/logins.json";
    private static final String saltUrl = "https://www.memoria-app.de/api/salts.json";
    private static final String signUpUrl = "https://www.memoria-app.de/registrieren/";

    private EditText password;
    private EditText mail;
    private TextView loginButton;
    private TextView signUp;
    private UserLoginTask mAuthTask = null;
    private SharedPreferences settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);

        loginButton = (TextView) findViewById(R.id.loginLoginTextView);
        password = (EditText) findViewById(R.id.loginPasswordEdit);
        mail = (EditText) findViewById(R.id.loginMailEdit);
        signUp = (TextView) findViewById(R.id.loginRegisterTextView);

        settings = getSharedPreferences("login", MODE_PRIVATE);
        if (settings.getBoolean("remember", false)) {
            password.setText(settings.getString("password",""));
            mail.setText(settings.getString("mail",""));
        }

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        loginButton.setOnClickListener(this);
        signUp.setOnClickListener(this);
        final CheckedTextView checkedTextView = (CheckedTextView) findViewById(R.id.loginSaveTextView);
        checkedTextView.setChecked(settings.getBoolean("remember", false));
        checkedTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = settings.edit();
                if (checkedTextView.isChecked()) {
                    editor.putBoolean("remember", false);
                    checkedTextView.setChecked(false);
                } else {
                    editor.putBoolean("remember", true);
                    checkedTextView.setChecked(true);
                }
                editor.commit();
            }
        });

        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nMgr.cancelAll();

        if (!Utils.isServiceRunning(this)) {
            Intent pushIntent = new Intent(this, ReminderService.class);
            this.startService(pushIntent);
        }
    }

    @Override
    public void onClick(View view) {
        if (view == loginButton) {

            SharedPreferences.Editor editor = settings.edit();
            // Logindaten speichern
            if(settings.getBoolean("remember", false)) {
                editor.putString("mail", mail.getText().toString());
                editor.putString("password", password.getText().toString());
                editor.commit();
            }

            mAuthTask = new UserLoginTask();
            mAuthTask.execute(
                    saltUrl,
                    loginUrl,
                    mail.getText().toString(),
                    password.getText().toString()
            );
        }
        else if (view == signUp) {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(signUpUrl));
            startActivity(i);
        }
    }

    public class UserLoginTask extends AsyncTask<String, Void, Boolean> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(LoginActivity.this , "",
                    "Check ...", true);
        }

        /**
         * param[0] = SaltUrl
         * param[1] = LoginUrl
         * param[2] = LoginName
         * param[3] = Password
         * @param params
         * @return
         */
        @Override
        protected Boolean doInBackground(String... params) {
            boolean retr = false;

            // Accept all certificates
        	// should be fixed in the future with the root cert
            try {
                SSLContext ctx = SSLContext.getInstance("TLS");
                ctx.init(null, new TrustManager[] {
                        new X509TrustManager() {
                            public void checkClientTrusted(X509Certificate[] chain, String authType) {}
                            public void checkServerTrusted(X509Certificate[] chain, String authType) {}
                            public X509Certificate[] getAcceptedIssuers() { return new X509Certificate[]{}; }
                        }
                }, null);

            } catch (Exception e) {
                e.printStackTrace();
            }

            Wrapper wrapper = null;
            HttpURLConnection conn = null;
            try {
                URL url = new URL(params[0]);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                conn.setRequestProperty("Accept", "application/json");

                OutputStream os = conn.getOutputStream();
                os.write(new String("email=" + params[2]).getBytes());
                os.flush();

                StringBuilder builder = new StringBuilder();
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    String line = "";
                    while ((line = br.readLine()) != null) {
                        builder.append(line);
                    }
                }

                String result = builder.toString();

                if (!TextUtils.isEmpty(result)) {
                    wrapper = getWrapper(result);
                    if(wrapper.getState() == 200) {
                        String salt = wrapper.getData().getAsJsonObject().get("salt").getAsString();

                        Log.d("Salt", salt);
                        if (salt != "" && !params[3].equals("")) {
                            url = new URL(params[1]);
                            conn = (HttpURLConnection) url.openConnection();
                            conn.setRequestMethod("POST");
                            conn.setDoOutput(true);
                            conn.setDoInput(true);
                            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                            conn.setRequestProperty("Accept", "application/json");

                            WsseToken token = new WsseToken(
                                    params[2],
                                    WsseToken.Hashing.sha512x5000(salt, params[3]),
                                    salt
                            );
                            Log.d(WsseToken.HEADER_WSSE, token.getWsseHeader());

                            conn.setRequestProperty(WsseToken.HEADER_WSSE, token.getWsseHeader());

                            OutputStream osLogin = conn.getOutputStream();
                            osLogin.write(new String("deviceId=" + ((TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId()).getBytes());
                            osLogin.flush();

                            try {
                                InputStream ip = conn.getInputStream();
                                BufferedReader br1 = new BufferedReader(new InputStreamReader((ip)));
                                if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                                    StringBuilder builderLogin = new StringBuilder();
                                    String line = "";
                                    while ((line = br1.readLine()) != null) {
                                        builderLogin.append(line);
                                    }

                                    Log.d("LoginActivity", builderLogin.toString());

                                    //Get user and set to instance
                                    wrapper = getWrapper(builderLogin.toString());
                                    if (wrapper.getState() == 200) {
                                        User user = User.getInstance();
                                        user.setName(wrapper.getData().getAsJsonObject().get("name").getAsString());
                                        user.setExpiresAt(wrapper.getData().getAsJsonObject().get("expires_at").getAsString());
                                        retr = true;
                                    } else
                                        showToast(wrapper.getMessage());
                                }
                            }
                            catch (FileNotFoundException e) {
                                showToast(getResources().getString(R.string.login_error_incorrect_password));
                            }
                        }
                    }
                    else
                        showToast(getResources().getString(R.string.login_error_incorrect_password));
                }
            }
            catch (FileNotFoundException e) {
                Log.e("FileNotFoundException", e.getMessage());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            finally {
                if (conn != null)
                    conn.disconnect();
            }
            return retr;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            super.onPostExecute(success);
            progressDialog.dismiss();
            mAuthTask = null;
            if (success) {
                Intent intent = new Intent(getApplicationContext(), ContactsListActivity.class);
                startActivity(intent);
                finish();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            progressDialog.dismiss();
        }
    }

    private static Wrapper getWrapper(String jsonString) {
        Wrapper wrapper = null;
        try {
            Gson gson = new Gson();
            wrapper = gson.fromJson(jsonString, Wrapper.class);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return wrapper;
    }

    private void showToast(final String message) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}
